﻿using Microsoft.AspNetCore.Http;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Threading.Tasks;

namespace AllepOrxan.Extentions
{
    public static class Extention
    {
        public static bool FileType(this IFormFile file,string type)
        {
            return file.ContentType.Contains(type);
        }
        public static bool FileSize(this IFormFile file,int kB)
        {
            return file.Length / 1024 > kB;
        }
        public async static Task<string> SaveFileAsync(this IFormFile file, string root,string folder)
        {
            string fileName = DateTime.Now.ToString("yyyyMMddHHmmss") + "_" + file.FileName.ToString();
            string resultPath = Path.Combine(root, folder, fileName);
            using(FileStream fileStream=new FileStream(resultPath, FileMode.Create))
            {
                await file.CopyToAsync(fileStream);
            }
            return fileName;
        }
        public static double Endirim(this double price, double c)
        {
            return (price * c / 100);
        }
    }
}
