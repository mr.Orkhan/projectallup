﻿using AllepOrxan.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace AllepOrxan.ViewModels
{
    public class CardVM
    {
        public int Id { get; set; }
        public string  Image { get; set; }
        public string Name { get; set; }
        public double Price { get; set; }
        public int Count { get; set; }
        public double Total { get; set; }
        public bool Delete { get; set; }
        public string Tags { get; set; }
        public string Description { get; set; }
        public ICollection<ProductImages> ProductImage { get; set; }
        public Product Product { get; set; }
    }
}
