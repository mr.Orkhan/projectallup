﻿using AllepOrxan.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace AllepOrxan.ViewModels
{
    public class BlogVM
    {
        public ICollection<BlogHome> BlogHomes { get; set; }
        public BlogHome BlogHome { get; set; }
        public Category Category { get; set; }
        public ICollection<Category> Categories { get; set; }
        public Comment Comment { get; set; }
        public ICollection<Comment> Comments { get; set; }
    }
}
