﻿using AllepOrxan.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace AllepOrxan.ViewModels
{
    public class ShopLeftSidebarVM
    {
       
        public ICollection<Product> Products { get; set; }
        
        public ICollection<ProductImages> ProductImages { get; set; }
        public Category Category { get; set; }
        public ICollection<Category> Categories { get; set; }
       
    }
}
