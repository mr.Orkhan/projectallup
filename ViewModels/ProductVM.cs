﻿using AllepOrxan.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace AllepOrxan.ViewModels
{
    public class ProductVM
    {
      
        public ICollection<Category> Categories { get; set; }
        public Comment Comment { get; set; }
        public ICollection<Comment> Comments { get; set; }
        public Product Product { get; set; }
        public ProductImages ProductImage { get; set; }
        public ICollection<Product> Products { get; set; }
        public ICollection<ProductImages> ProductImages { get; set; }
    }
}
