﻿using AllepOrxan.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace AllepOrxan.ViewModels
{
    public class HeaderVM
    {
        public ICollection<About2> About2s { get; set; }
        public ICollection<About3> About3s { get; set; }
        public ICollection<Category> Categories { get; set; }
        public Product Product { get; set; }
        public Category Category { get; set; }
        public ICollection<Product> Products { get; set; }
       
    }
}
