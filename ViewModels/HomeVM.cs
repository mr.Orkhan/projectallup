﻿using AllepOrxan.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace AllepOrxan.ViewModels
{
    public class HomeVM
    {
        public ICollection<Category> Categories { get; set; }
        public Product Product { get; set; }
        public ProductImages ProductImage { get; set; }
        public ICollection<Product> Products { get; set; }
        public ICollection<ProductImages> ProductImages { get; set; }
        public ICollection<BlogHome> BlogHomes { get; set; }
       
        public ICollection<Slider> Sliders { get; set; }
    }
}
