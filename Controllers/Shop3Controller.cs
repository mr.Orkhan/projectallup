﻿using AllepOrxan.DAL;
using AllepOrxan.Models;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace AllepOrxan.Controllers
{
    public class Shop3Controller : Controller
    {
        private readonly AppDbContext _context;
        public Shop3Controller(AppDbContext context)
        {
            _context = context;
        }
        public async Task<IActionResult> Index()
        {
            List<Product> products = await _context.Products.Include(p => p.ProductImages).ToListAsync();
            return View(products);
        }
    }
}
