﻿using AllepOrxan.DAL;
using AllepOrxan.Models;
using AllepOrxan.ViewModels;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace AllepOrxan.Controllers
{
    public class BlogController : Controller
    {
        private readonly AppDbContext _context;
        public BlogController(AppDbContext context)
        {
            _context = context;
        }
        public async Task<IActionResult> Index(int? id)
        {
            Category category = await _context.Categories.Include(p=>p.BlogHomes).FirstOrDefaultAsync(c=>c.Id==id);
            BlogVM blogVM = new BlogVM
            {
               
                Category = category,
                BlogHomes = await _context.BlogHomes.ToListAsync(),
                Categories = await _context.Categories.ToListAsync()
            };
            return View(blogVM);
        }
       
        public async Task<IActionResult> Single(int? id)
        {

            BlogHome blogHomes = await _context.BlogHomes.FindAsync(id);
            blogHomes.Comments = await _context.Comments.Where(p => p.BlogHomeId == blogHomes.Id).ToListAsync();


            if (blogHomes == null)
            {
                return NotFound();
            }
            else
            {



            }
            BlogVM blogVM = new BlogVM
            {
              Comments=blogHomes.Comments,
                Comment = await _context.Comments.FirstOrDefaultAsync(),
                BlogHomes = await _context.BlogHomes.Include(p => p.Category).ToListAsync(),
                BlogHome = blogHomes,
                 Category = await _context.Categories.Include(p=>p.BlogHomes).Include(p => p.ProductCategories).ThenInclude(p => p.Product).FirstOrDefaultAsync()

        };
            return View(blogVM);
           
        }
        [HttpPost]
        [ValidateAntiForgeryToken]
        [ActionName("Single")]
        public async Task<IActionResult> SinglePost(int? id, Comment comment)
        {

            await _context.Comments.AddAsync(comment);
            await _context.SaveChangesAsync();
            return View();



        }
        public async Task<IActionResult> Bloges()
        {
            BlogHome blogHome = await _context.BlogHomes.Include(p => p.Category).FirstOrDefaultAsync();
            return PartialView("_BlogPartial", blogHome);
        }
        public async Task<IActionResult> CategoryBlog(int? id)
        {
            List<Category> category = await _context.Categories.Include(c => c.BlogHomes).ThenInclude(b=>b.CategoryId==id).ToListAsync();

            return PartialView("_BlogCategoryPartial", category);
        }
        public async Task<IActionResult> Search(string search)
        {
            return Json(search);
        }
    }
}
