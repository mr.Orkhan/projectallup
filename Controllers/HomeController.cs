﻿using AllepOrxan.DAL;
using AllepOrxan.Models;
using AllepOrxan.ViewModels;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Identity;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace AllepOrxan.Controllers
{
    public class HomeController : Controller
    {
        private readonly AppDbContext _context;
        private readonly UserManager<AppUser> _userManager;
        public HomeController(AppDbContext context, UserManager<AppUser> userManager)
        {
            _context = context;
            _userManager = userManager;
        }
        public async Task<IActionResult> Index()
        {
            List<Product> products = await _context.Products.Include(p => p.ProductImages).ToListAsync();
            //foreach (Product product in products)
            //{
            //    product.ProductImages = await _context.ProductImages.Where(p => p.ProductId == product.Id).ToListAsync();
            //}

            HomeVM homeVM = new HomeVM
            {
                Categories = await _context.Categories.Where(c => c.IsDeleted == false && c.IsMain).ToListAsync(),
                Products = products,
                             
                Sliders = await _context.Sliders.ToArrayAsync()
            };
            return View(homeVM);
        }
        public async Task<IActionResult> AddCard(int? id)
        {

            if (id == null)
            {
                return NotFound();
            }
            Product product = await _context.Products.FindAsync(id);

            if (product == null)
            {
                return NotFound();
            }


            List<CardVM> cardVMs;

            if (Request.Cookies["card"] == null)
            {
                cardVMs = new List<CardVM>();
            }
            else
            {
                cardVMs = JsonConvert.DeserializeObject<List<CardVM>>(Request.Cookies["card"]);
            }
            CardVM excard = cardVMs.Find(c => c.Id == id);
            if (excard != null)
            {
                excard.Count++;
            }
            else
            {
                product.ProductImages = await _context.ProductImages.Where(p => p.ProductId == product.Id).ToListAsync();

                CardVM newcardVMs = new CardVM
                {
                    Id = product.Id,

                    Count = product.Quantity

                };
                cardVMs.Add(newcardVMs);
            }
            string cookies = JsonConvert.SerializeObject(cardVMs);
            Response.Cookies.Append("card", cookies, new CookieOptions { MaxAge = TimeSpan.FromDays(30) });

            return RedirectToAction(nameof(Index));
        }
       
        
        public async Task<IActionResult> Card()
        {

            //if (!User.Identity.IsAuthenticated)
            //{
            //    return RedirectToAction("Login", "Account");
            //}

            string cookies = Request.Cookies["card"];
            List<CardVM> cardVMs;
            if (cookies != null)
            {
                cardVMs = JsonConvert.DeserializeObject<List<CardVM>>(cookies);
            }
            else
            {
                cardVMs = new List<CardVM>();
            }
            foreach (CardVM item in cardVMs)
            {

               

                Product product = await _context.Products.FindAsync(item.Id);
                product.ProductImages = await _context.ProductImages.Where(p => p.ProductId == product.Id).ToListAsync();
                item.Name = product.Name;
                item.Price = product.Price;
                item.Product = product;
                item.ProductImage = product.ProductImages;


            }
            return View(cardVMs);
        }

        
        public async Task<IActionResult> AddWishlist(int? id)
        {

            if (id == null)
            {
                return NotFound();
            }
            Product product = await _context.Products.FindAsync(id);

            if (product == null)
            {
                return NotFound();
            }


            List<CardVM> cardVMs;

            if (Request.Cookies["wishlist"] == null)
            {
                cardVMs = new List<CardVM>();
            }
            else
            {
                cardVMs = JsonConvert.DeserializeObject<List<CardVM>>(Request.Cookies["wishlist"]);
            }
            CardVM excard = cardVMs.Find(c => c.Id == id);
            if (excard != null)
            {
                excard.Count++;
            }
            else
            {
                product.ProductImages = await _context.ProductImages.Where(p => p.ProductId == product.Id).ToListAsync();
                
                CardVM newcardVMs = new CardVM
                {
                    Id = product.Id,

                    Count = product.Quantity

                };
                cardVMs.Add(newcardVMs);
            }
            string cookies = JsonConvert.SerializeObject(cardVMs);
            Response.Cookies.Append("wishlist", cookies, new CookieOptions { MaxAge = TimeSpan.FromDays(30) });

            return RedirectToAction(nameof(Index));
        }
    
        public async Task<IActionResult> Wishlist()
        {

            //if (!User.Identity.IsAuthenticated)
            //{
            //    return RedirectToAction("Login", "Account");
            //}

            string cookies = Request.Cookies["wishlist"];
            List<CardVM> cardVMs;
            if (cookies != null)
            {
                cardVMs = JsonConvert.DeserializeObject<List<CardVM>>(cookies);
            }
            else
            {
                cardVMs = new List<CardVM>();
            }
            foreach (CardVM item in cardVMs)
            {


                Product product = await _context.Products.FindAsync(item.Id);
                product.ProductImages = await _context.ProductImages.Where(p => p.ProductId == product.Id).ToListAsync();
                item.Name = product.Name;
                item.Price = product.Price;

                item.ProductImage = product.ProductImages;


            }
            return View(cardVMs);
        }

        public async Task<IActionResult> AddCompare(int? id)
        {

            if (id == null)
            {
                return NotFound();
            }
            Product product = await _context.Products.FindAsync(id);
           
            if (product == null)
            {
                return NotFound();
            }


            List<CardVM> cardVMs;

            if (Request.Cookies["compare"] == null)
            {
                cardVMs = new List<CardVM>();
            }
            else
            {
                cardVMs = JsonConvert.DeserializeObject<List<CardVM>>(Request.Cookies["compare"]);
            }
            CardVM excard = cardVMs.Find(c => c.Id == id);
            if (excard != null)
            {
                excard.Count++;
            }
            else
            {
                product.ProductImages = await _context.ProductImages.Where(p => p.ProductId == product.Id).ToListAsync();
                
                CardVM newcardVMs = new CardVM
                {
                    Id = product.Id,

                    Count = product.Quantity

                };
                cardVMs.Add(newcardVMs);
            }
            string cookies = JsonConvert.SerializeObject(cardVMs);
            Response.Cookies.Append("compare", cookies, new CookieOptions { MaxAge = TimeSpan.FromDays(30) });

            return RedirectToAction(nameof(Index));
        }
  
        public async Task<IActionResult> Compare()
        {

            //if (!User.Identity.IsAuthenticated)
            //{
            //    return RedirectToAction("Login", "Account");
            //}

            string cookies = Request.Cookies["compare"];
            List<CardVM> cardVMs;
            if (cookies != null)
            {
                cardVMs = JsonConvert.DeserializeObject<List<CardVM>>(cookies);
            }
            else
            {
                cardVMs = new List<CardVM>();
            }
            foreach (CardVM item in cardVMs)
            {



                Product product = await _context.Products.FindAsync(item.Id);
                product.ProductImages = await _context.ProductImages.Where(p => p.ProductId == product.Id).ToListAsync();
                item.Name = product.Name;
                item.Price = product.Price;
                item.Tags = product.Tags;
                item.Description = product.Description;
                item.ProductImage = product.ProductImages;


            }
            return View(cardVMs);
        }

        public async Task<IActionResult> Departmen()
        {
            List<Category> category = await _context.Categories.Include(c => c.ProductCategories).ToListAsync();
            return PartialView("_DepartmenPartial", category);
        }
        public async Task<IActionResult> AddCheckout(int? id)
        {

            if (id == null)
            {
                return NotFound();
            }
            Product product = await _context.Products.FindAsync(id);

            if (product == null)
            {
                return NotFound();
            }


            List<CardVM> cardVMs;

            if (Request.Cookies["checkout"] == null)
            {
                cardVMs = new List<CardVM>();
            }
            else
            {
                cardVMs = JsonConvert.DeserializeObject<List<CardVM>>(Request.Cookies["checkout"]);
            }
            CardVM excard = cardVMs.Find(c => c.Id == id);
            if (excard != null)
            {
                excard.Count++;
            }
            else
            {
                product.ProductImages = await _context.ProductImages.Where(p => p.ProductId == product.Id).ToListAsync();

                CardVM newcardVMs = new CardVM
                {
                    Id = product.Id,

                    Count = product.Quantity

                };
                cardVMs.Add(newcardVMs);
            }
            string cookies = JsonConvert.SerializeObject(cardVMs);
            Response.Cookies.Append("checkout", cookies, new CookieOptions { MaxAge = TimeSpan.FromDays(30) });

            return RedirectToAction(nameof(Index));
        }

        public async Task<IActionResult> Checkout()
        {

            //if (!User.Identity.IsAuthenticated)
            //{
            //    return RedirectToAction("Login", "Account");
            //}

            string cookies = Request.Cookies["checkout"];
            List<CardVM> cardVMs;
            if (cookies != null)
            {
                cardVMs = JsonConvert.DeserializeObject<List<CardVM>>(cookies);
            }
            else
            {
                cardVMs = new List<CardVM>();
            }
            foreach (CardVM item in cardVMs)
            {


                Product product = await _context.Products.FindAsync(item.Id);
                
                item.Name = product.Name;
                item.Price = product.Price;

                


            }
            return View(cardVMs);
        }
        [HttpPost]
        [ValidateAntiForgeryToken]
        [ActionName("Checkout")]
        public async Task<IActionResult> CheckoutPost()
        {
            //if (!User.Identity.IsAuthenticated)
            //{
            //    return RedirectToAction("Login", "Account");
            //}
            AppUser user = await _userManager.FindByNameAsync(User.Identity.Name);
            List<CardVM> cardVMs;
            if (Request.Cookies["checkout"] != null)
            {
                cardVMs = JsonConvert.DeserializeObject<List<CardVM>>(Request.Cookies["checkout"]);
            }
            else
            {
                return RedirectToAction("Index", "Home");
            }

            List<SaleProduct> saleProducts = new List<SaleProduct>();
            double total = 0;
            foreach (CardVM cardVM in cardVMs)
            {
                Product product = await _context.Products.FirstOrDefaultAsync();
                if (product.Count <= cardVM.Count)
                {
                    return RedirectToAction("Basket");
                }
                SaleProduct saleProduct = new SaleProduct
                {
                    Price = product.Price,
                    ProductId = product.Id,
                    Count = cardVM.Count
                };

                saleProducts.Add(saleProduct);

                total += saleProduct.Price * saleProduct.Count;
            }
            Sale sale = new Sale
            {
                AppUserId = user.Id,
                Date = DateTime.UtcNow.AddHours(4),
                SaleProducts = saleProducts,
                Total = total
            };

            await _context.Sales.AddAsync(sale);
            await _context.SaveChangesAsync();
            return RedirectToAction("Index");
        }


    }
}
