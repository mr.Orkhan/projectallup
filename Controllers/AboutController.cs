﻿using AllepOrxan.DAL;
using AllepOrxan.Models;
using AllepOrxan.ViewModels;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace AllepOrxan.Controllers
{
    public class AboutController : Controller
    {
        private readonly AppDbContext _context;
        public AboutController(AppDbContext context)
        {
            _context = context;
        }
        public async Task<IActionResult> Index()
        {
            List<About> about = await _context.Abouts.ToListAsync();
           
            return View(about);
        }
    }
}
