﻿using AllepOrxan.DAL;
using AllepOrxan.Models;
using AllepOrxan.ViewModels;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Identity;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace AllepOrxan.Controllers
{
    public class ProductController : Controller
    {
        private readonly AppDbContext _context;
        private readonly SignInManager<AppUser> _signInManager;
        private readonly UserManager<AppUser> _userManager;
        public ProductController(AppDbContext context, UserManager<AppUser> userManager, SignInManager<AppUser> signInManager)
        {
            _context = context;
            _signInManager = signInManager;
            _userManager = userManager;
        }
        public async  Task<IActionResult> Index(int? id )
        {
            List<Product> products = await _context.Products.Include(p => p.ProductImages).ToListAsync();
            foreach (Product productc in products)
            {
                productc.ProductImages = await _context.ProductImages.Where(p => p.ProductId == productc.Id).ToListAsync();
            }
            if (id == null) return NotFound();
            Product product = await _context.Products.FindAsync(id);
            product.ProductImages =await _context.ProductImages.Where(p => p.ProductId == product.Id).ToListAsync();
            product.Comments= await _context.Comments.Where(p => p.ProductId == product.Id).ToListAsync();

            if (product == null)
            {
                return NotFound();
            }
            else
            {
              
                    
               
            }
           
            ProductVM productVM = new ProductVM
            {


                Comment=await _context.Comments.FirstOrDefaultAsync(),
                Product = product,
                Products = products
             
            };
            return View(productVM);

        }
        public async Task<IActionResult> SearchCategories(string search)
        {
            List<Category> category = await _context.Categories.Include(c => c.ProductCategories).ThenInclude(p=>p.Product).ToListAsync();
           


            return PartialView("_SearchCategoryPartial", category);
        }

        public async Task<IActionResult> Search(string search)
        {
           
            List<Product> products = await _context.Products.Include(p => p.ProductCategories)
                .Where(p => p.Name.Contains(search))
                .OrderByDescending(p => p.Id)
                .Take(4).ToListAsync();
           
            
           


            return PartialView("_SearchProductPartial", products);
        }
        [HttpPost]
        [ValidateAntiForgeryToken]
        [ActionName("Index")]
        public async Task<IActionResult> IndexPost(int? id,Comment comment)
        {
            
            await _context.Comments.AddAsync(comment);
            await _context.SaveChangesAsync();
            return View();



        }
    }
}
