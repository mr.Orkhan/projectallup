﻿using AllepOrxan.DAL;
using AllepOrxan.Models;
using AllepOrxan.ViewModels;
using Microsoft.AspNetCore.Identity;
using Microsoft.AspNetCore.Mvc;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace AllepOrxan.Controllers
{
    public class MyAccountController : Controller
    {
        private readonly AppDbContext _context;
        private readonly UserManager<AppUser> _userManager;
        public MyAccountController(AppDbContext context, UserManager<AppUser> userManager)
        {
            _context = context;
            _userManager = userManager;
        }
        public async Task<IActionResult> Index()
        {
            if (!User.Identity.IsAuthenticated)
            {
                return RedirectToAction("Login", "Account");
            }

            string cookies = Request.Cookies["checkout"];
            List<CardVM> cardVMs;
            if (cookies != null)
            {
                cardVMs = JsonConvert.DeserializeObject<List<CardVM>>(cookies);
            }
            else
            {
                cardVMs = new List<CardVM>();
            }
            foreach (CardVM item in cardVMs)
            {


                Product product = await _context.Products.FindAsync(item.Id);

                item.Name = product.Name;
                item.Price = product.Price;




            }
            return View(cardVMs);
            
        }
    }
}
