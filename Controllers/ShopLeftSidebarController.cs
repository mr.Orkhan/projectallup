﻿using AllepOrxan.DAL;
using AllepOrxan.Models;
using AllepOrxan.ViewModels;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace AllepOrxan.Controllers
{
    public class ShopLeftSidebarController : Controller
    {
        private readonly AppDbContext _context;
        public ShopLeftSidebarController(AppDbContext context)
        {
            _context = context;
        }
        public async Task<IActionResult> Index(int? id)
        {
            if (id == null) return View();
           
            
            Category category = await _context.Categories.Include(p => p.ProductCategories).ThenInclude(p=>p.Product).FirstOrDefaultAsync(c => c.IsDeleted == false && c.Id == id);            
           
            if (category == null)
            {
                return NotFound();
            }
            ShopLeftSidebarVM shopLeftSidebarVM = new ShopLeftSidebarVM
            {
                Category = category,
                ProductImages = await _context.ProductImages.ToListAsync(),
                Categories = await _context.Categories.Where(c => c.IsDeleted == false && c.IsMain).Include(c => c.Children).ToListAsync()
            };
            return View(shopLeftSidebarVM);
        }
        public async Task<IActionResult> Shop4()
        {
            List<Product> products = await _context.Products.Include(p => p.ProductImages).ToListAsync();
            return View(products);
        }
        public async Task<IActionResult> Category()
        {
            Category category = await _context.Categories.Include(p => p.ProductCategories).FirstOrDefaultAsync();
            return PartialView("_ShopPartial", category);
        }
        public async Task<IActionResult> Product()
        {
            Product product = await _context.Products.FirstOrDefaultAsync();
            return PartialView("_ShopProductPartial", product);
        }
        //public async Task<IActionResult> ProductMoney(int? mainCategoryId)
        //{
        //    if (mainCategoryId == null)
        //    {

        //        return View();
        //    }
        //    Category mainCategory = await _context.Categories.Include(c => c.Children).FirstOrDefaultAsync(c => c.IsDeleted == false && c.IsMain && c.Id == mainCategoryId);
        //    if (mainCategory == null)
        //    {

        //        return View();
        //    }
        //    return PartialView("_ChildCategoryPartial", mainCategory.Children.Where(c => c.IsDeleted == false));
        //}
    }
}
