﻿using AllepOrxan.Models;
using Microsoft.AspNetCore.Identity.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace AllepOrxan.DAL
{
    public class AppDbContext:IdentityDbContext<AppUser>
    {
        public AppDbContext(DbContextOptions<AppDbContext> options):base(options)
        {

        }
        public virtual  DbSet<Category> Categories { get; set; }
        public virtual DbSet<About> Abouts { get; set; }
        public virtual DbSet<About2> About2s { get; set; }
        public virtual DbSet<About3> About3s { get; set; }
        public virtual DbSet<Product> Products { get; set; }
        public virtual DbSet<ProductImages> ProductImages { get; set; }
        public virtual DbSet<Comment> Comments { get; set; }
        public virtual DbSet<ProductCategory> ProductCategories { get; set; }
        public virtual DbSet<BlogHome> BlogHomes { get; set; }
        public virtual DbSet<Slider> Sliders { get; set; }
        public virtual DbSet<SaleProduct> SaleProducts { get; set; }
        public virtual DbSet<Sale> Sales { get; set; }
    }
}
