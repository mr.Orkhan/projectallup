﻿using AllepOrxan.DAL;
using AllepOrxan.Extentions;
using AllepOrxan.Helpers;
using AllepOrxan.Models;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.ModelBinding;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Threading.Tasks;

namespace AllepOrxan.Areas.Admin.Controllers
{
    [Area("Admin")]
    
    public class AboutController : Controller
    {
        private readonly AppDbContext _context;
        private readonly IWebHostEnvironment _env;
        public AboutController(AppDbContext context, IWebHostEnvironment env)
        {
            _context = context;
            _env = env;
        }
        public async Task<IActionResult> Index()
        {
            List<About> about=await _context.Abouts.ToListAsync();
            return View(about);
        }

        public IActionResult Create()
        {

            return View();
        }
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> Create(About about)
        {
            if (ModelState["Photo"].ValidationState == ModelValidationState.Invalid)
            {
                return View();
            }
            if (!about.Photo.FileType("image/"))
            {
                ModelState.AddModelError("Photo", "Duzgun tip secin");
                return View();
            }
            if (about.Photo.FileSize(300))
            {
                ModelState.AddModelError("Photo", "Olcusu bizi asar!!!");
                return View();

            }
           
               
            
            string Image = await about.Photo.SaveFileAsync(_env.WebRootPath, Path.Combine("assets", "images"));

            about.Image = Image;
            await _context.Abouts.AddAsync(about);
            await _context.SaveChangesAsync();
            return RedirectToAction("Index");

        }
        public async Task<IActionResult> Update(int? id)
        {
            if (id == null) return NotFound();
            About about = await _context.Abouts.FindAsync(id);
            if (about == null) return NotFound();
            return View(about);
        }
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> Update(int? id, About about)
        {

            if (id == null) return NotFound();
            About about1 = await _context.Abouts.FindAsync(id);
            if (about1 == null) return NotFound();

            if (ModelState["Photo"].ValidationState == ModelValidationState.Invalid)
            {
                return View();
            }
            if (!about.Photo.FileType("image/"))
            {
                ModelState.AddModelError("Photo", "Duzgun photo daxil edin");
                return View();
            }
            if (about.Photo.FileSize(200))
            {
                ModelState.AddModelError("Photo", "olcu bizi asar");
                return View();
            }
            Helper.DeleteFile(about1.Image, _env.WebRootPath, Path.Combine("assets", "images"));
            string Image = await about.Photo.SaveFileAsync(_env.WebRootPath, Path.Combine("assets", "images"));
            about1.Image = Image;
            about1.Title = about.Title;
            about1.Text = about.Text;
            about1.Tesminoals = about.Tesminoals;
            about1.OurTeam = about.OurTeam;
            about1.Company = about.Company;
            await _context.SaveChangesAsync();

            return RedirectToAction("Index");
        }
        public async Task<IActionResult> Delete(int? id)
        {
            if (id == null) return NotFound();
            About about = await _context.Abouts.FindAsync(id);
            if (about == null) return NotFound();
            return View(about);
        }
        [HttpPost]
        [ValidateAntiForgeryToken]
        [ActionName("Delete")]
        public async Task<IActionResult> DeleteAbout(int? id)
        {
            if (id == null) return NotFound();
            About about = await _context.Abouts.FindAsync(id);
            if (about == null) return NotFound();

            Helper.DeleteFile(about.Image, _env.WebRootPath, Path.Combine("assets", "images"));
            _context.Abouts.Remove(about);
            await _context.SaveChangesAsync();

            return RedirectToAction("Index");
        }
    }
}
