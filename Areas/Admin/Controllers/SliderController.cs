﻿using AllepOrxan.DAL;
using AllepOrxan.Extentions;
using AllepOrxan.Helpers;
using AllepOrxan.Models;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.ModelBinding;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Threading.Tasks;

namespace AllepOrxan.Areas.Admin.Controllers
{
    [Area("Admin")]
   
    public class SliderController : Controller
    {
        private readonly AppDbContext _context;
        private readonly IWebHostEnvironment _env;
        public SliderController(AppDbContext context, IWebHostEnvironment env)
        {
            _context = context;
            _env = env;
        }
        public async Task<IActionResult> Index()
        {
            List<Slider> slider = await _context.Sliders.ToListAsync();
            return View(slider);
        }
        public async Task<IActionResult> Detail(int? id)
        {
            if (id == null) return NotFound();
            Slider slider = await _context.Sliders.FindAsync(id);
            if (slider == null) return NotFound();
            return View(slider);
        }
        public IActionResult Create()
        {
           
            return View();
        }
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> Create(Slider slider)
        {
            if (ModelState["Photo"].ValidationState == ModelValidationState.Invalid)
            {
                return View();
            }
            if (!slider.Photo.FileType("image/"))
            {
                ModelState.AddModelError("Photo", "Duzgun tip secin");
                return View();
            }
            if (slider.Photo.FileSize(300))
            {
                ModelState.AddModelError("Photo", "Olcusu bizi asar!!!");
                return View();

            }
            if (await _context.Sliders.AnyAsync(s=>s.Name.ToLower()==slider.Name.ToLower()))
            {
                ModelState.AddModelError("Name", "Artiq movcuddur");
                return View();
            }
            string Image = await slider.Photo.SaveFileAsync(_env.WebRootPath, Path.Combine("assets", "images"));

            slider.Image = Image;
            await _context.Sliders.AddAsync(slider);
            await _context.SaveChangesAsync();
            return RedirectToAction(nameof(Index));

        }
        //[HttpPost]
        //[ValidateAntiForgeryToken]
        //public async Task<IActionResult> Create(Slider slider)
        //{
        //    if (ModelState["Photos"].ValidationState == ModelValidationState.Invalid)
        //    {
        //        return View();
        //    }
        //    foreach (IFormFile Photo in slider.Photos)
        //    {

        //        if (!Photo.FileType("image/"))
        //        {
        //            ModelState.AddModelError("Photos", "Duzgun photo daxil edin");
        //            return View();
        //        }
        //        if (Photo.FileSize(200))
        //        {
        //            ModelState.AddModelError("Photos", "olcu bizi asar");
        //            return View();
        //        }

        //        Slider newSlider = new Slider
        //        {
        //            Image = await Photo.SaveFile(_env.WebRootPath, Path.Combine("assets", "images"))

        //        };

        //        await _context.Sliders.AddAsync(newSlider);
        //        await _context.SaveChangesAsync();
        //    }
        //    return RedirectToAction(nameof(Index));
        //}

        public async Task<IActionResult> Update(int? id)
        {
            if (id == null) return NotFound();
            Slider slider = await _context.Sliders.FindAsync(id);
            if (slider == null) return NotFound();
            return View(slider);
        }
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> Update(int? id,Slider slider)
        {

            if(id == null) return NotFound();
            Slider dbslider = await _context.Sliders.FindAsync(id);
            if (dbslider == null) return NotFound();

            if (ModelState["Photo"].ValidationState == ModelValidationState.Invalid)
            {
                return View();
            }
            if (!slider.Photo.FileType("image/"))
            {
                ModelState.AddModelError("Photo", "Duzgun photo daxil edin");
                return View();
            }
            if (slider.Photo.FileSize(200))
            {
                ModelState.AddModelError("Photo", "olcu bizi asar");
                return View();
            }
            Helper.DeleteFile(dbslider.Image, _env.WebRootPath, Path.Combine("assets", "images"));
            string Image = await slider.Photo.SaveFileAsync(_env.WebRootPath, Path.Combine("assets", "images"));
            dbslider.Image = Image;
            dbslider.Name = slider.Name;
            dbslider.FadeInDown = slider.FadeInDown;
            dbslider.FadeInLeft = slider.FadeInLeft;

            await _context.SaveChangesAsync();

            return RedirectToAction(nameof(Index));
        }

        public async Task<IActionResult> Delete(int? id)
        {
            if (id == null) return NotFound();
            Slider slider = await _context.Sliders.FindAsync(id);
            if (slider == null) return NotFound();
            return View(slider);
        }
        [HttpPost]
        [ValidateAntiForgeryToken]
        [ActionName("Delete")]
        public async Task<IActionResult> DeleteSlider(int? id)
        {
            if (id == null) return NotFound();
            Slider slider = await _context.Sliders.FindAsync(id);
            if (slider == null) return NotFound();

            Helper.DeleteFile(slider.Image, _env.WebRootPath, Path.Combine("assets", "images"));
            _context.Sliders.Remove(slider);
            await _context.SaveChangesAsync();

            return RedirectToAction(nameof(Index));
        }
    }
}
