﻿using AllepOrxan.DAL;
using AllepOrxan.Extentions;
using AllepOrxan.Helpers;
using AllepOrxan.Models;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.ModelBinding;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Threading.Tasks;

namespace AllepOrxan.Areas.Admin.Controllers
{
    [Area("Admin")]
   
    public class LogoController : Controller
    {
        private readonly AppDbContext _context;
        private readonly IWebHostEnvironment _env;
        public LogoController(AppDbContext context, IWebHostEnvironment env)
        {
            _context = context;
            _env = env;
        }
        public async Task<IActionResult> Index()
        {
            return View(await _context.About3s.ToListAsync());
        }

        public async Task<IActionResult> Create()
        {
            return View();
        }
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> Create(About3 about)
        {
            if (ModelState["Photo"].ValidationState == ModelValidationState.Invalid)
            {
                return View();
            }
            if (!about.Photo.FileType("image/"))
            {
                ModelState.AddModelError("Photo", "Duzgun tip secin");
                return View();
            }
            if (about.Photo.FileSize(300))
            {
                ModelState.AddModelError("Photo", "Olcusu bizi asar!!!");
                return View();

            }
            if (about.Adress == null)
            {
                ModelState.AddModelError("Adress", "Unvan qeyd edin!!!");
                return View();
            }
            string Image = await about.Photo.SaveFileAsync(_env.WebRootPath, Path.Combine("assets", "images", "brand"));
            about.Logo = Image;
            await _context.About3s.AddAsync(about);
            await _context.SaveChangesAsync();
            return RedirectToAction(nameof(Index));
        }
        public async Task<IActionResult> Detail(int? id)
        {
            if (id == null) return NotFound();
            About3 about = await _context.About3s.FindAsync(id);
            if (about == null) return NotFound();
            return View(about);
        }
        public async Task<IActionResult> Update(int? id)
        {
            if (id == null) return NotFound();
            About3 about = await _context.About3s.FindAsync(id);
            if (about == null) return NotFound();
            return View(about);
        }
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> Update(int? id, About3 about)
        {
            if (id == null) return NotFound();
            About3 dbabout = await _context.About3s.FindAsync(id);
            if (dbabout == null) return NotFound();
            if (ModelState["Photo"].ValidationState == ModelValidationState.Invalid)
            {
                return View();
            }
            if (!about.Photo.FileType("image/"))
            {
                ModelState.AddModelError("Photo", "Duzgun tip secin");
                return View();
            }
            if (about.Photo.FileSize(300))
            {
                ModelState.AddModelError("Photo", "Olcusu bizi asar!!!");
                return View();

            }
            Helper.DeleteFile(dbabout.Logo, _env.WebRootPath, Path.Combine("assets", "images", "brand"));
            string Image = await about.Photo.SaveFileAsync(_env.WebRootPath, Path.Combine("assets", "images", "brand"));
            dbabout.Logo = Image;
            dbabout.Adress = about.Adress;
            await _context.SaveChangesAsync();
            return RedirectToAction(nameof(Index));
        }
        public async Task<IActionResult> Delete(int? id)
        {
            if (id == null) return NotFound();
            About3 about = await _context.About3s.FindAsync(id);
            if (about == null) return NotFound();
            return View(about);
        }
        [HttpPost]
        [ValidateAntiForgeryToken]
        [ActionName("Delete")]
        public async Task<IActionResult> DeleteLogo(int? id)
        {
            if (id == null) return NotFound();
            About3 about = await _context.About3s.FindAsync(id);
            if (about == null) return NotFound();
            Helper.DeleteFile(about.Logo, _env.WebRootPath, Path.Combine("assets", "images"));
            _context.About3s.Remove(about);
            await _context.SaveChangesAsync();

            return RedirectToAction(nameof(Index));
        }
    }
}
