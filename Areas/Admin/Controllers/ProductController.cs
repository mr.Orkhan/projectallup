﻿using AllepOrxan.DAL;
using AllepOrxan.Extentions;
using AllepOrxan.Helpers;
using AllepOrxan.Models;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Threading.Tasks;

namespace AllepOrxan.Areas.Admin.Controllers
{
    [Area("Admin")]
   
    public class ProductController : Controller
    {
        private readonly AppDbContext _context;
        private readonly IWebHostEnvironment _env;
        public ProductController(AppDbContext context, IWebHostEnvironment env)
        {
            _context = context;
            _env = env;
        }
        public async Task<IActionResult> Index()
        {
            return View(await _context.Products.Include(p => p.ProductImages).Include(p => p.ProductCategories).ThenInclude(p => p.Category).Where(p => p.Delete == false).ToListAsync());
        }
        public async Task<IActionResult> Create()
        {
         
            return View();
        }
        [HttpPost]
        [ValidateAntiForgeryToken]

        public async Task<IActionResult> Create(Product product)// int? mainCategoryId, int? childCategoryId)
        {
           
            double inter = product.Interest;
            product.newPrice = product.Price-((product.Price * inter) / 100);
            if (!ModelState.IsValid)
            {
                return View();
            }
           
            if (product.Photos == null)
            {
                ModelState.AddModelError("Photos", "Sekildaxilet");
                return View();
            }
            if (await _context.Products.AnyAsync(c => c.Delete == false && c.Name.ToLower().Trim() == product.Name.ToLower().Trim()))
            {
                ModelState.AddModelError("Name", "Bu adda Kategory movcuddur");
                return View();
            }

            List<ProductImages> productImages = new List<ProductImages>();
            foreach (IFormFile photo in product.Photos)
            {
                if (!photo.FileType("image/"))
                {
                    ModelState.AddModelError("Photo", "Duzgun photo daxil edin");
                    return View();
                }
                if (photo.FileSize(200))
                {
                    ModelState.AddModelError("Photo", "olcu bizi asar");
                    return View();
                }
                ProductImages productImage = new ProductImages
                {
                    Images = await photo.SaveFileAsync(_env.WebRootPath, Path.Combine("assets", "images", "product"))

                };

                productImages.Add(productImage);
            }
         
            product.ProductImages = productImages;
         


            await _context.Products.AddAsync(product);
            await _context.SaveChangesAsync();
            return RedirectToAction(nameof(Index));
        }
       
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> DeleteImage(int? id)
        {
            if (id == null) return NotFound();
            ProductImages productImages = await _context.ProductImages.FindAsync(id);
            if (productImages == null) return NotFound();
            Product product = await _context.Products.Include(p => p.ProductImages).FirstOrDefaultAsync(c => c.Delete == false && c.Id == productImages.ProductId);
            if (product.ProductImages.Count() == 1)
            {
                return RedirectToAction(nameof(Update), new { id = productImages.ProductId });
            }
            Helper.DeleteFile(productImages.Images, _env.WebRootPath, Path.Combine("assetts", "images", "product"));

            _context.ProductImages.Remove(productImages);
            await _context.SaveChangesAsync();
            return RedirectToAction(nameof(Update), new { id = productImages.ProductId });
        }
        public async Task<IActionResult> Update(int? id)
        {
            if (id == null) return NotFound();
            Product product = await _context.Products.Include(p => p.ProductCategories).ThenInclude(p => p.Category).Include(p => p.ProductImages).FirstOrDefaultAsync(c => c.Id == id && c.Delete == false);
            if (product == null) return NotFound();
         
            return View(product);
        }
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> Update(int? id, Product product)
        {
            if (id == null) return NotFound();
            Product dbproduct = await _context.Products.Include(p => p.ProductCategories).ThenInclude(p => p.Category).Include(p => p.ProductImages).FirstOrDefaultAsync(c => c.Id == id && c.Delete == false);
            if (dbproduct == null) return NotFound();

          

            if (!ModelState.IsValid)
            {
                product.ProductImages = dbproduct.ProductImages;
                product.ProductCategories = dbproduct.ProductCategories;
                return View(product);
            }
         
            if (!await _context.Categories.AnyAsync(c => c.IsDeleted == false && c.IsMain ))
            {
                product.ProductImages = dbproduct.ProductImages;
                product.ProductCategories = dbproduct.ProductCategories;
                ModelState.AddModelError("", "Duzgun Main Category sec");
                return View(product);
            }
          
            if (await _context.Products.AnyAsync(c => c.Delete == false && c.Name.ToLower().Trim() == product.Name.ToLower().Trim() && product.Id != dbproduct.Id))

            {
                product.ProductImages = dbproduct.ProductImages;
                product.ProductCategories = dbproduct.ProductCategories;
                ModelState.AddModelError("Name", "Bu adda Kategory movcuddur");
                return View(product);
            }

            foreach (IFormFile photo in product.Photos)
            {
                if (!photo.FileType("image/"))
                {
                    ModelState.AddModelError("Photo", "Duzgun photo daxil edin");
                    return View();
                }
                if (photo.FileSize(200))
                {
                    ModelState.AddModelError("Photo", "olcu bizi asar");
                    return View();
                }
                ProductImages productImage = new ProductImages
                {
                    Images = await photo.SaveFileAsync(_env.WebRootPath, Path.Combine("assets", "images", "product"))
                };
                dbproduct.ProductImages.Add(productImage);
            }
           
       
            dbproduct.Name = product.Name;
            dbproduct.Quantity = product.Quantity;
            dbproduct.Description = product.Description;
            dbproduct.Price = product.Price;
            dbproduct.Image = product.Image;
            dbproduct.Extax = product.Extax;
            dbproduct.Brand = product.Brand;
            dbproduct.Tags = product.Tags;
            dbproduct.ProductCode = product.ProductCode;
            dbproduct.Rate = product.Rate;
            dbproduct.SaleCount = product.SaleCount;

            await _context.SaveChangesAsync();
            return RedirectToAction(nameof(Index));
        }
        public async Task<IActionResult> Detail(int? id)
        {

            if (id == null) return NotFound();
            Product product = await _context.Products.Include(p => p.ProductCategories).ThenInclude(p => p.Category).Include(p => p.ProductImages).FirstOrDefaultAsync(c => c.Id == id && c.Delete == false);
            if (product == null) return NotFound();
            return View(product);
        }
        public async Task<IActionResult> Delete(int? id)
        {
            if (id == null) return NotFound();
            Product product = await _context.Products.Include(p => p.ProductCategories).ThenInclude(p => p.Category).Include(p => p.ProductImages).FirstOrDefaultAsync(c => c.Id == id && c.Delete == false);
            if (product == null) return NotFound();
            return View(product);
        }
        [HttpPost]
        [ValidateAntiForgeryToken]
        [ActionName("Delete")]
        public async Task<IActionResult> Deleteproduct(int? id, int? mainCategoryId, int? childCategoryId)
        {
            if (id == null) return NotFound();
            Product product = await _context.Products.Include(p => p.ProductCategories).ThenInclude(p => p.Category).Include(p => p.ProductImages).FirstOrDefaultAsync(c => c.Id == id && c.Delete == false);
            if (product == null) return NotFound();
           
            _context.Products.Remove(product);
            await _context.SaveChangesAsync();

            return RedirectToAction(nameof(Index));
        }
    }
}

