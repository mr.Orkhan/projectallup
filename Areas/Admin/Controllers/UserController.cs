﻿using AllepOrxan.DAL;
using AllepOrxan.Models;
using AllepOrxan.ViewModels;
using Microsoft.AspNetCore.Identity;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace AllepOrxan.Areas.Admin.Controllers
{
    [Area("Admin")]
   
    public class UserController : Controller
    {
        private readonly AppDbContext _context;
        private readonly UserManager<AppUser> _userManager;
        public UserController(AppDbContext context, UserManager<AppUser> userManager)
        {
            _context = context;
            _userManager = userManager;
        }
        public async Task< IActionResult> Index()
        {
            List<AppUser> appusers = await _userManager.Users.ToListAsync();
            List<UserVM> userVMs= new List<UserVM>();
            foreach (AppUser appuser in appusers)
            {
                UserVM userVM = new UserVM
                {
                    Id = appuser.Id,
                    FullName = appuser.FullName,
                    Email = appuser.Email,
                    UserName = appuser.UserName,
                    IsDeleted = appuser.IsDeleted,
                    Role = (await _userManager.GetRolesAsync(appuser))[0]
                };
                userVMs.Add(userVM);
            }



            return View(userVMs);
        }
        public async Task<IActionResult> ChangeStatus(string id)
        {
            if (id==null)
            {
                return NotFound();
            }
            AppUser appUser = await _userManager.FindByIdAsync(id);
            if (appUser == null)
            {
                return NotFound();
            }
            return View(appUser);
        }
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> ChangeStatus(string id,bool Status)
        {
            if (id == null)
            {
                return NotFound();
            }
            AppUser appUser = await _userManager.FindByIdAsync(id);
            if (appUser == null)
            {
                return NotFound();
            }
            appUser.IsDeleted = Status;
            await _context.SaveChangesAsync();
            return RedirectToAction(nameof(Index));
        }
        public async Task<IActionResult> ChangeRole(string id)
        {
            if (id == null)
            {
                return NotFound();
            }
            AppUser appUser = await _userManager.FindByIdAsync(id);
            if (appUser == null)
            {
                return NotFound();
            }
            UserVM userVM = new UserVM
            {
                Id = appUser.Id,
                FullName = appUser.FullName,
                Email = appUser.Email,
                UserName = appUser.UserName,
                IsDeleted = appUser.IsDeleted,
                Role = (await _userManager.GetRolesAsync(appUser))[0],
                Roles=new List<string> { "Admin","Member"}
            };
                
            return View(userVM);
        }
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> ChangeRole(string id, string Role)
        {
            if (id == null)
            {
                return NotFound();
            }
            AppUser appUser = await _userManager.FindByIdAsync(id);
            if (appUser == null)
            {
                return NotFound();
            }
            string oldRole = (await _userManager.GetRolesAsync(appUser))[0];
            //await _userManager.RemoveFromRolesAsync(appUser, oldRole);
            //await _userManager.AddToRolesAsync(appUser, Role);
            return RedirectToAction(nameof(Index));
        }
        public async Task<IActionResult> ChangePassword(string id)
        {
            if (id == null)
            {
                return NotFound();
            }
            AppUser appUser = await _userManager.FindByIdAsync(id);
            if (appUser == null)
            {
                return NotFound();
            }
            return View(appUser);
        }
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> ChangePassword(string id,string NewPassword)
        {
            if (id == null)
            {
                return NotFound();
            }
            AppUser appUser = await _userManager.FindByIdAsync(id);
            if (appUser == null)
            {
                return NotFound();
            }
            string token = await _userManager.GeneratePasswordResetTokenAsync(appUser);
            await _userManager.ResetPasswordAsync(appUser, token, NewPassword);
            return RedirectToAction(nameof(Index));
        }
    }
}
