﻿using AllepOrxan.DAL;
using AllepOrxan.Extentions;
using AllepOrxan.Models;
using AllepOrxan.ViewModels;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.ModelBinding;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Threading.Tasks;

namespace AllepOrxan.Areas.Admin.Controllers
{
    [Area("Admin")]
   
    public class CategoryController : Controller
    {
        private readonly AppDbContext _context;
        private readonly IWebHostEnvironment _env;
        public CategoryController(AppDbContext context, IWebHostEnvironment env)
        {
            _context = context;
            _env = env;
        }
        public async Task<IActionResult> Index()
        {
            return View(await _context.Categories.Where(c=>c.IsDeleted==false).ToListAsync());
        }
        public async Task<IActionResult> Create()
        {
         
            return View();
        }
        [HttpPost]
        [ValidateAntiForgeryToken]
        
        public async Task<IActionResult> Create(Category category)
        {
         
            if (!ModelState.IsValid)
            {
                return View(category);
            }
            if (category.IsMain)
            {
                if (await _context.Categories.AnyAsync(c=>c.IsMain && c.IsDeleted==false && c.Name.ToLower().Trim()== category.Name.ToLower().Trim()))
                {
                    ModelState.AddModelError("Name","Bu adda Kategory movcuddur");
                    return View();
                }
                if (category.Photo==null)
                {
                    ModelState.AddModelError("Photo", "photo mutleq secilmelidir");
                    return View(category);
                }
                if (!category.Photo.FileType("image/"))
                {
                    ModelState.AddModelError("Photo", "Duzgun photo daxil edin");
                    return View(category);
                }
                if (category.Photo.FileSize(200))
                {
                    ModelState.AddModelError("Photo", "olcu bizi asar");
                    return View(category);
                }
                string fileFolder = Path.Combine("assets", "images");
                category.Image = await category.Photo.SaveFileAsync(_env.WebRootPath, fileFolder);
            }
           
            await _context.Categories.AddAsync(category);
            await _context.SaveChangesAsync();
            return RedirectToAction(nameof(Index));
        }
        public async Task<IActionResult> Detail(int? id)
        {
          
  
            if (id == null) return NotFound();
            Category category = await _context.Categories.Include(c => c.Children).Include(c => c.Parent).FirstOrDefaultAsync(c => c.Id == id && c.IsDeleted == false);
            if (category == null) return NotFound();

            ViewBag.Parent = await _context.Categories.FindAsync(category.ParentId);
       
            return View(category);
        }
        public async Task<IActionResult> Delete(int? id)
        {
           
            
            if (id == null) return NotFound();
            Category category = await _context.Categories.Include(c => c.Children).Include(c => c.Parent).FirstOrDefaultAsync(c => c.Id == id && c.IsDeleted == false);
            if (category == null) return NotFound();
            return View(category);
        }
        [HttpPost]
        [ValidateAntiForgeryToken]
        [ActionName("Delete")]
        public async Task<IActionResult> DeleteCategory(int? id)
        {
            
            if (id == null) return NotFound();
            Category category = await _context.Categories.Include(c => c.Children).Include(c => c.Parent).FirstOrDefaultAsync(c => c.Id == id && c.IsDeleted==false);

            if (category == null) return NotFound();

           

            if (category.IsMain)
            {
               
                foreach (Category child in category.Children.Where(c=>!c.IsDeleted))
                {
                    child.IsDeleted = true;
                }
            }
            category.IsDeleted=true;
            await _context.SaveChangesAsync();
            return RedirectToAction(nameof(Index));
        }
        public async Task<IActionResult> Update(int? id)
        {
          
            if (id == null) return NotFound();
            Category category = await _context.Categories.FindAsync(id);
            if (category == null) return NotFound();
            ViewBag.Parent = await _context.Categories.FindAsync(category.ParentId);
            return View(category);

        }
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> Update(int? id, Category category)
        {
          
            if (id == null) return NotFound();
            Category dbcategory = await _context.Categories.FindAsync(id);

            if (dbcategory == null) return NotFound();
            if (dbcategory.IsMain)
            {
                if (ModelState["Photo"].ValidationState==ModelValidationState.Invalid)
                {
                    return View();
                }
                if (category.Photo == null)
                {
                    ModelState.AddModelError("Photo", "photo mutleq secilmelidir");
                    return View(category);
                }
                if (!category.Photo.FileType("image/"))
                {
                    ModelState.AddModelError("Photo", "Duzgun photo daxil edin");
                    return View(category);
                }
                if (category.Photo.FileSize(200))
                {
                    ModelState.AddModelError("Photo", "olcu bizi asar");
                    return View(category);
                }
                string fileFolder = Path.Combine("assets", "images");
                Helpers.Helper.DeleteFile(_env.WebRootPath, fileFolder, dbcategory.Image);
                string Image =await category.Photo.SaveFileAsync(_env.WebRootPath, fileFolder);
                dbcategory.Image = Image;
                dbcategory.IsMain = true;
            }

            if (await _context.Categories.AnyAsync(c =>c.Name.ToLower().Trim() == category.Name.ToLower().Trim() && c.Id!=category.Id))
            {
                ModelState.AddModelError("Name", "Bu adda Kategory movcuddur");
                return View(dbcategory);
            }
            dbcategory.Name = category.Name;
            await _context.SaveChangesAsync();
            return RedirectToAction(nameof(Index));
        }

    }
}
