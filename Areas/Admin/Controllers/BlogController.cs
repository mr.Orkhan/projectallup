﻿using AllepOrxan.DAL;
using AllepOrxan.Extentions;
using AllepOrxan.Helpers;
using AllepOrxan.Models;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.ModelBinding;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Threading.Tasks;

namespace AllepOrxan.Areas.Admin.Controllers
{
    [Area("Admin")]
    public class BlogController : Controller
    {
        private readonly AppDbContext _context;
        private readonly IWebHostEnvironment _env;
        public BlogController(AppDbContext context, IWebHostEnvironment env)
        {
            _context = context;
                _env = env;
        }
        public async Task<IActionResult> Detail(int? id)
        {
            if (id == null) return NotFound();
            BlogHome blogHome = await _context.BlogHomes.FindAsync(id);
            if (blogHome == null) return NotFound();
            return View(blogHome);
        }
        public async Task<IActionResult> Index()
        {

            List<BlogHome> blogHomes = await _context.BlogHomes.ToListAsync();
            return View(blogHomes);
        }
        public async  Task<IActionResult> Create()
        {
            ViewBag.MainCategories = await _context.Categories.Where(c => c.IsDeleted == false && c.IsMain).ToListAsync();
            return View();
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> Create(BlogHome blogHome, int? mainCategoryId)
        {
            ViewBag.MainCategories = await _context.Categories.Where(c => c.IsDeleted == false && c.IsMain).ToListAsync();
            if (ModelState["Photo"].ValidationState == ModelValidationState.Invalid)
            {
                return View();
            }
            if (!blogHome.Photo.FileType("image/"))
            {
                ModelState.AddModelError("Photo", "Duzgun tip secin");
                return View();
            }
            if (blogHome.Photo.FileSize(300))
            {
                ModelState.AddModelError("Photo", "Olcusu bizi asar!!!");
                return View();

            }
            if (await _context.BlogHomes.AnyAsync(s => s.Title.ToLower() == blogHome.Title.ToLower()))
            {
                ModelState.AddModelError("Name", "Artiq movcuddur");
                return View();
            }
            string Image = await blogHome.Photo.SaveFileAsync(_env.WebRootPath, Path.Combine("assets", "images"));
            await _context.BlogHomes.Include(c => c.Category).FirstOrDefaultAsync(c =>  c.CategoryId == mainCategoryId);
              Category mainCategory = await _context.Categories.Include(c => c.Children).FirstOrDefaultAsync(c => c.IsMain && c.IsDeleted == false && c.Id == mainCategoryId);
                if (mainCategory==null)
                {
                    return NotFound();
                }
            blogHome.Image = Image;
            blogHome.CategoryId = mainCategory.Id;

            await _context.BlogHomes.AddAsync(blogHome);
            await _context.SaveChangesAsync();
            return RedirectToAction(nameof(Index));

        }
        public async Task<IActionResult> Delete(int? id)
        {
            if (id == null) return NotFound();
            BlogHome blogHome = await _context.BlogHomes.FindAsync(id);
            if (blogHome == null) return NotFound();
            return View(blogHome);
        }
        [HttpPost]
        [ValidateAntiForgeryToken]
        [ActionName("Delete")]
        public async Task<IActionResult> DeleteBlog(int? id)
        {
            if (id == null) return NotFound();
            BlogHome blogHome = await _context.BlogHomes.FindAsync(id);
            if (blogHome == null) return NotFound();

            Helper.DeleteFile(blogHome.Image, _env.WebRootPath, Path.Combine("assets", "images"));
            _context.BlogHomes.Remove(blogHome);
            await _context.SaveChangesAsync();

            return RedirectToAction(nameof(Index));
        }
        public async Task<IActionResult> Update(int? id)
        {
            if (id == null) return NotFound();
            BlogHome blogHome = await _context.BlogHomes.FindAsync(id);
            if (blogHome == null) return NotFound();
            return View(blogHome);
        }
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> Update(int? id, BlogHome blogHome, int? mainCategoryId)
        {

            if (id == null) return NotFound();
            BlogHome dbblogHome = await _context.BlogHomes.FindAsync(id);
            if (dbblogHome == null) return NotFound();

            ViewBag.MainCategories = await _context.Categories.Where(c => c.IsDeleted == false && c.IsMain).ToListAsync();
            if (ModelState["Photo"].ValidationState == ModelValidationState.Invalid)
            {
                return View();
            }
            if (!blogHome.Photo.FileType("image/"))
            {
                ModelState.AddModelError("Photo", "Duzgun photo daxil edin");
                return View();
            }
            if (blogHome.Photo.FileSize(200))
            {
                ModelState.AddModelError("Photo", "olcu bizi asar");
                return View();
            }
            Helper.DeleteFile(dbblogHome.Image, _env.WebRootPath, Path.Combine("assets", "images"));
            string Image = await blogHome.Photo.SaveFileAsync(_env.WebRootPath, Path.Combine("assets", "images"));
            dbblogHome.Image = Image;
            dbblogHome.Title = blogHome.Title;
            dbblogHome.Description = blogHome.Description;
            dbblogHome.CategoryId = blogHome.CategoryId;

            dbblogHome.History = blogHome.History;
            await _context.SaveChangesAsync();

            return RedirectToAction(nameof(Index));
        }
    }
}
