﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;

namespace AllepOrxan.Models
{
    public class Comment
    {
        public int Id { get; set; }
        public string Name { get; set; }
        [Required, EmailAddress, DataType(DataType.EmailAddress)]
        public string Email { get; set; }
       
        public string Comments { get; set; }
        public int ProductId { get; set; }
        public virtual Product Product { get; set; }
        public int BlogHomeId { get; set; }
        public virtual BlogHome BlogHome { get; set; }

    }
}
