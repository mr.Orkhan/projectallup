﻿using Microsoft.AspNetCore.Http;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Threading.Tasks;

namespace AllepOrxan.Models
{
    public class About3
    {
        public int Id { get; set; }
        public string Logo { get; set; }
        public string Adress { get; set; }
        [NotMapped]
        [Required]
        public IFormFile Photo { get; set; }
    }
}
