﻿using Microsoft.AspNetCore.Http;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Threading.Tasks;

namespace AllepOrxan.Models
{
    public class Slider
    {
        public int Id { get; set; }
        public string FadeInDown { get; set; }
        public string FadeInLeft { get; set; }
        public string Name { get; set; }
        public string Image { get; set; }
        [NotMapped]
        [Required]
        public IFormFile Photo { get; set; }
        [NotMapped]
        [Required]
        public IFormFile[] Photos { get; set; }
    }
}
