﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace AllepOrxan.Models
{
    public class ProductImages
    {
        public int Id { get; set; }
        
        public string Images { get; set; }
        public int ProductId { get; set; }
        public virtual Product Product { get; set; }
        public bool IsDeleted { get; set; }

      
    }
}
