﻿using Microsoft.AspNetCore.Http;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Threading.Tasks;

namespace AllepOrxan.Models
{
    public class Product
    {
        public int Id { get; set; }
        [Required]
        public string Name { get; set; }
        public int Quantity { get; set; }
        public string  Description { get; set; }
        [Required]
        public double Price { get; set; }
        public double newPrice { get; set; }
        public double Interest { get; set; }
        [NotMapped]
        public ProductImages Image { get; set; }
        public double Extax { get; set; }
        public string Brand { get; set; }
        public string Tags  { get; set; }
        public bool Delete { get; set; }
        public string ProductCode { get; set; }
        public int Rate { get; set; }
        public int SaleCount { get; set; }
        public int Count { get; set; }
       
        public string Title { get; set; }
        public bool IsFeatured { get; set; }
        public Nullable<double> Discount { get; set; }
        [NotMapped]
        [Required]
        public IFormFile[] Photos { get; set; }
        public virtual ICollection<ProductImages> ProductImages { get; set; }

        public virtual ICollection<Comment> Comments { get; set; }
        public virtual ICollection<ProductCategory> ProductCategories { get; set; }
        public virtual ICollection<SaleProduct> SaleProducts { get; set; }

        internal object Where(Func<object, bool> p)
        {
            throw new NotImplementedException();
        }
    }
}
