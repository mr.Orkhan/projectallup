﻿using Microsoft.AspNetCore.Http;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Threading.Tasks;

namespace AllepOrxan.Models
{
    public class About
    {
        public int Id { get; set; }
        public string Image { get; set; }
        public string Title { get; set; }
        public string OurTeam { get; set; }
        public string Tesminoals { get; set; }
        public string Company { get; set; }
        [NotMapped]
        [Required]
        public IFormFile Photo { get; set; }
        public string Text { get; set; }

      
    }
}
