﻿using Microsoft.AspNetCore.Http;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Threading.Tasks;

namespace AllepOrxan.Models
{
    public class Category
    {
        public int Id { get; set; }
        [Required]
        public string   Name { get; set; }
        public string Image { get; set; }
        [NotMapped]
      
        public IFormFile Photo { get; set; }
        public bool IsDeleted { get; set; }
        public bool IsMain { get; set; }
        public int? ParentId { get; set; }
        public virtual Category Parent { get; set; }
        public virtual ICollection<Category> Children { get; set; }
        public virtual ICollection<BlogHome> BlogHomes { get; set; }
        public virtual ICollection<ProductCategory> ProductCategories { get; set; }
    }
}
