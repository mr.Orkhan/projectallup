﻿using AllepOrxan.DAL;
using AllepOrxan.ViewModels;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace AllepOrxan.ViewComponents
{
    public class FooterViewComponent: ViewComponent
    {
        private readonly AppDbContext _context;
        public FooterViewComponent(AppDbContext context)
        {
            _context = context;
        }
        public async Task<IViewComponentResult> InvokeAsync()
        {
            HeaderVM headerVM = new HeaderVM
            {
                About2s = await _context.About2s.ToListAsync(),

                About3s = await _context.About3s.ToListAsync()
            };
            return View(await Task.FromResult(headerVM));
        }
    }
}
